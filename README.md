## INFO

Da ich die GUI leider nicht zu 100% fertig bekommen habe, sind die Daten der Prozesse in der Konsole (F12 unter dem reiter Konsole) als Datensatz (COLUMN ARRAY) ausgegeben.

## Installation

Um das Projekt aufsetzen zu können, muss Node.js installiert sein.
Danach muss im Verzeichnis von dem Projekt folgender Befehl im Terminal ausgeführt werden:

### `npm ci`

Das könnte einige Minuten in Anspruch nehmen.

## Start

Um sich das Projekt nun anschauen zu können, muss ebenfalls im Terminal aus dem Projekt-Verzeichnis folgender Befehl ausgeführt werden:

### `npm start`

Nun sollte sich automatisch der Standard-Browser öffnen und das Projekt aufrufen. Das könnte ebenfalls etwas dauern.
Falls das nicht der Fall ist, kann das Projekt unter der URL http://localhost:3000/ erreicht werden.

## Nutzung

Sobald das Programm benutzt wird, sollte man sich noch die folgenden Dinge anschauen und auch beachten.

### Erstellen eines Netzplans:

In dem Feld 'Bezeichnung' kann eingegeben werden, was man möchte.

In dem Feld 'Prozessschritt' müssen Zahlen verwendet werden. Diese sollte im besten Fall der Nummer von dem Prozess entsprechen. Also für den ersten Prozess = 1, für den zweiten = 2 usw.

In dem Feld 'Dauer in Stunden' müssen ebenfalls Zahlen benutzt werden. Diese Zahl entspricht der Dauer in Stunden von dem jeweiligen Prozess.

Bei dem Feld 'Vorher zu beenden' ist zu beachten, dass bei dem ersten Prozess ein '-' angegeben ist.
Bei den anderen Prozessen werden Zahlen verwendet. Falls ein Prozess mehrere Vorgänger haben sollte, werden die Zahlen mit einem Komma separiert (1,2,3 etc.).

Um weitere Prozesse anzulegen, muss man auf 'Prozess hinzufügen' drücken.

Sobald man alle Prozesse angelegt hat, drückt man auf 'Netzplan erstellen', um den Netzplan zu generieren.
Die angelegten Prozesse können später geändert werden. <br/>
WICHTIG: Es kann kein einzelner Prozess gelöscht werden!<br/>
WICHTIG: Der Netzplan darf nicht mit zwei verschiedenen Prozessen enden.

### Netzplan editieren

Um den Netzplan editieren zu können, muss man oben rechts auf das Symbol mit dem Blatt und dem Stift drücken. Danach wird einem das Formular mit den bereits vorhandenen Daten angezeigt, welche dann editiert werden können.
Man kann auch weitere Prozesse anlegen.
Bei den Feldern gelten die selben Regeln, wie beim erstellen eines neuen Netzplans.

### Neuen Netzplan erstellen

Um einen neuen Netzplan erstellen zu können, muss man oben rechts auf das Symbol mit dem + drücken.
Danach wird einem das Formular zum erstellen eines neuen Netzplans angezeigt.
