import React from 'react';
import './App.css';
import CreateNewNetworkPlan from "./components/CreateNewNetworkPlan";
import NetworkPlan from "./components/NetworkPlan";
import CalculateProcesses from "./components/CalculateProcesses";
import HeaderMenu from "./components/HeaderMenu";

import 'semantic-ui-css/semantic.min.css';
import EditNetworkPlan from "./components/EditNetworkPlan";

/**
 * Interface für die Props.
 */
interface AppProps {
}

/**
 * Interface für den State.
 */
interface AppState {
    lastUpdated: string
}

export default class App extends React.Component<AppProps, AppState> {

    /**
     * Speicher für die ausgewählten Daten.
     */
    private storage = localStorage;

    /**
     * Angezeigtes HTML, Standard = <CreateNewNetworkPlan/>.
     */
    private content: React.ReactNode = <CreateNewNetworkPlan getNetworkPlanData={this.networkPlanData.bind(this)}/>;

    /**
     * Array, welches die Daten, welche der Benutzer eingegeben hat, beinhaltet.
     */
    private setData: Array<any> = [];

    /**
     * Konstruktor der Klasse.
     *
     * @param props
     */
    constructor(props: any) {
        super(props);

        // State der Klasse, wird benutzt, um den DOM zu aktualisieren.
        this.state = {
            lastUpdated: new Date().toLocaleString()
        };

        this.storageCleared = this.storageCleared.bind(this);
        this.changeNetworkPlan = this.changeNetworkPlan.bind(this);
    }

    /**
     * Rendert den NetzpLan direkt nachdem <CreateNewNetworkPlan/> ausgefüllt und bestätigt wurde.
     *
     * @param data
     */
    public networkPlanData(data: Array<any>): void {

        // Wird gebraucht, wenn man den Netzplan editieren will.
        this.setData = data;

        // Instanz der 'CalculateProcesses'-Klasse, welche die Daten für den Netzplan erechnet.
        let calculate = new CalculateProcesses(data);

        // Die fertig ausgerechneten Prozesse.
        let processes = calculate.getProcesses();

        localStorage.clear();

        // Speichert die fertigen Prozesse.
        this.storage.setItem('networkPlanData', JSON.stringify(processes));

        // Rendert den Netzplan.
        this.content =
            <div>
                <HeaderMenu createNewNetworkPlan={this.storageCleared} editCurrentNetworkPlan={this.changeNetworkPlan}/>
                <div className={'plan'}>
                    <NetworkPlan networkPlanData={processes}/>
                </div>
            </div>;

        // Aktualisiert den DOM.
        this.setState({lastUpdated: new Date().toLocaleString()});
    }

    /**
     * Ändert die Variable content zu <CreateNewNetworkPlan getNetworkPlanData={this.networkPlanData.bind(this)}/>
     * wenn der Storage geleert wurde.
     */
    public storageCleared(): void {
        this.content = <CreateNewNetworkPlan getNetworkPlanData={this.networkPlanData.bind(this)}/>;
        this.setState({lastUpdated: new Date().toLocaleString()});
    }

    /**
     * Ändert die Variable content zu <EditNetworkPlan setData={this.setData} getNetworkPlanData={this.networkPlanData.bind(this)}/>
     * wenn der Editieren-Button gedrückt wurde.
     */
    public changeNetworkPlan(): void {
        this.content = <EditNetworkPlan setData={this.setData} getNetworkPlanData={this.networkPlanData.bind(this)}/>;
        this.setState({lastUpdated: new Date().toLocaleString()});
    }

    /**
     * Standard Render-Methode von React.
     * Rendert das HTML.
     *
     * Wird immer neu ausgeführt, wenn sich der State der Klasse aktualisiert.
     */
    public render(): React.ReactNode {

        // Wenn der Speicher nicht leer ist und somit Daten vorhanden hat,
        // wird 'content' zu dem Netzplan geändert, welcher mit den vorhandenen Daten gefüllt wird.
        if (localStorage.length !== 0) {
            this.content =
                <div>
                    <HeaderMenu createNewNetworkPlan={this.storageCleared}
                                editCurrentNetworkPlan={this.changeNetworkPlan}/>
                    <div className={'plan'}>
                        <NetworkPlan networkPlanData={JSON.parse(this.storage.getItem('networkPlanData') || '{}')}/>
                    </div>
                </div>;
        }

        return (
            <div className="App">
                {this.content}
            </div>
        );
    }
}