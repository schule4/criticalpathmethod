/**
 * Interface für Prozesse.
 */
interface Process {
    faz: (number | undefined),
    fez: (number | undefined),
    saz: (number | undefined),
    sez: (number | undefined),
    step: number,
    name: string,
    executionTime: number,
    totalBuffer: (number | undefined),
    freeBuffer: (number | undefined),
    previousProcesses: Array<any>,
    nextProcesses: Array<any>,
    isCritical: boolean
}

export default class CalculateProcesses {

    /**
     * Array, welches die fertigen Prozesse beinhaltet.
     */
    private processes: Array<any> = [];

    public constructor(dataFromCreateForm: Array<any>) {
        this.initProcesses(dataFromCreateForm);
    }

    /**
     * Erstell ein Array mit den Prozessen und ruft die Methoden auf, welche die Prozesse berechnen.
     *
     * @param processesData
     */
    public initProcesses(processesData: Array<any>): void {

        // Für jeden angelegten Prozess ein erweitertes Objekt erstellen.
        processesData.forEach((processData) => {

            let process: Process = {
                faz: undefined,
                fez: undefined,
                saz: undefined,
                sez: undefined,
                step: processData.step,
                name: processData.name,
                executionTime: processData.executionTime,
                totalBuffer: undefined,
                freeBuffer: undefined,
                previousProcesses: processData.finishFirst,
                nextProcesses: [],
                isCritical: false
            };

            this.processes.push(process);
        });

        this.setNextProcesses();
        this.vorwaertsTerminierung();
        this.rueckwaertTerminierung();
        this.calculateBuffer();
        this.setCritical();

    }

    /**
     * Setzt die Prozesse, welche von einem jeweiligen Prozess aus folgen.
     */
    public setNextProcesses(): void {
        this.processes.forEach((process) => {
            if (process.previousProcesses.length === 1) {
                if (process.previousProcesses[0] !== '-') {
                    this.processes[process.previousProcesses[0] - 1].nextProcesses.push(process.step);
                }
            } else {
                process.previousProcesses.forEach((prevProcess: any, index: number) => {
                    this.processes[process.previousProcesses[index] - 1].nextProcesses.push(process.step);
                });
            }
        });
    }

    /**
     * Durchläuft alle Prozesse und setzt den FAZ und den FEZ.
     */
    public vorwaertsTerminierung(): void {
        this.processes.forEach((process, index) => {
            if (index === 0) {
                process.faz = 0;
                process.fez = process.executionTime;
            } else {
                if (process.previousProcesses.length === 1) {
                    this.processes.forEach((singleProcess) => {

                        if (singleProcess.step === parseInt(process.previousProcesses[0])) {
                            let faz = singleProcess.fez;
                            let fez = faz + process.executionTime;

                            process.faz = faz;
                            process.fez = fez;
                        }
                    });

                } else {
                    let fazArray: Array<number> = [];

                    process.previousProcesses.forEach((prevProcess: any) => {
                        fazArray.push(this.processes[prevProcess - 1].fez);
                    });

                    let faz = Math.max(...fazArray);

                    process.faz = faz;
                    process.fez = faz + process.executionTime;
                }
            }
        });
    }

    /**
     * Durchläuft alle Prozesse und setzt den SEZ und den SAZ.
     */
    public rueckwaertTerminierung(): void {
        for (let i = this.processes.length - 1; i >= 0; i--) {

            if (i === this.processes.length - 1) {

                let sez = this.processes[i].fez;
                this.processes[i].sez = sez;
                this.processes[i].saz = sez - this.processes[i].executionTime;

            } else {
                if (this.processes[i].nextProcesses.length === 1) {
                    this.processes.forEach((singleProcess) => {

                        if (singleProcess.step === parseInt(this.processes[i].nextProcesses[0])) {
                            let sez = singleProcess.saz;
                            let saz = sez - this.processes[i].executionTime;

                            this.processes[i].sez = sez;
                            this.processes[i].saz = saz;
                        }
                    });

                } else {
                    let sezArray: Array<number> = [];

                    this.processes[i].nextProcesses.forEach((nextProcess: any) => {
                        sezArray.push(this.processes[nextProcess - 1].saz);
                    });

                    let sez = Math.min(...sezArray);

                    this.processes[i].sez = sez;
                    this.processes[i].saz = sez - this.processes[i].executionTime;
                }
            }
        }
    }

    /**
     * Berechnet den Puffer.
     */
    public calculateBuffer(): void {
        this.processes.forEach((process) => {
            process.totalBuffer = process.saz - process.faz;

            if (process.nextProcesses.length === 1) {
                this.processes.forEach((otherProcess: any) => {
                    if (otherProcess.step === parseInt(process.nextProcesses[0])) {
                        process.freeBuffer = otherProcess.faz - process.fez;
                    }
                });
            } else if (process.nextProcesses.length === 0) {
                process.freeBuffer = 0;
            } else {
                let fazArray: Array<any> = [];

                this.processes.forEach((otherProcess: any) => {
                    if (otherProcess.step === parseInt(process.nextProcesses[0])) {
                        fazArray.push(otherProcess.faz);
                    }
                });

                let faz = Math.min(...fazArray);

                process.freeBuffer = faz - process.fez;
            }

        });
    }

    /**
     * Gibt an, ob ein Prozess kritisch ist.
     */
    public setCritical(): void {
        this.processes.forEach((process) => {
            if (process.totalBuffer === 0 && process.freeBuffer === 0) {
                process.isCritical = true;
            }
        });
    }

    /**
     * Gibt das Array mit den Prozessen zurück.
     */
    public getProcesses(): Array<object> {
        return this.processes;
    }
}