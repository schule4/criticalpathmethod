import React from 'react';
import './css/CreateNewNetworkPlan.css';

/**
 * Props der Klasse.
 */
interface CreateNewNetworkPlanProps {
    getNetworkPlanData: (data: Array<any>) => void
}

/**
 * State der Klasse.
 */
interface CreateNewNetworkPlanState {
    lastUpdated: string
}

export default class CreateNewNetworkPlan extends React.Component<CreateNewNetworkPlanProps, CreateNewNetworkPlanState> {

    /**
     * Array, welches alle Prozess-Daten beinhaltet.
     */
    private processesData: Array<any> = [];

    /**
     * Hilfs-Variable für 'processList'.
     */
    private processListIndex: number = 1;

    /**
     * Array dessen Länge bestimmt, wie viele Prozesse angelegt werden sollen.
     */
    private processList: Array<number> = [];

    /**
     * HTML für die Input-Felder, welche einen Prozess anlegen.
     */
    private processes: React.ReactNode;

    /**
     * Konstruktor der Klasse.
     *
     * @param props
     */
    public constructor(props: any) {
        super(props);

        this.state = {
            lastUpdated: new Date().toLocaleString()
        };

        this.handleProcess = this.handleProcess.bind(this);

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    /**
     * Fügt Felder hinzu, um einen weiteren Prozess anlegen zu können.
     */
    public addProcess(): void {
        this.processList.push(this.processListIndex);
        this.processListIndex++;

        this.processes = this.processList.map(processNumber => {
            return (
                <div>
                    <input name={'processName_' + processNumber}
                           type={'text'}
                           placeholder={'Bezeichnung'}
                           onChange={this.handleProcess}
                    />
                    <input name={'processStep_' + processNumber}
                           type={'text'}
                           placeholder={'Prozessschritt'}
                           onChange={this.handleProcess}
                    />
                    <input name={'processExecutionTime_' + processNumber} type={'text'}
                           placeholder={'Dauer in Stunden'}
                           onChange={this.handleProcess}/>
                    <input name={'processToFinishFirst_' + processNumber} type={'text'}
                           placeholder={'Vorher zu beenden'}
                           onChange={this.handleProcess}/>
                </div>
            );
        });

        this.setState({lastUpdated: new Date().toLocaleString()});
    }

    /**
     * Speichert alle Daten in einem dazugehörigen Objekt aus einem Array ab.
     *
     * @param event
     */
    public handleProcess(event: any): void {

        let eventName = event.target.name;
        let eventValue = event.target.value;

        let processId = eventName.split('_')[1];

        if (this.processesData[processId] === undefined) {
            this.processesData.push({});
        }

        switch (eventName) {
            case 'processName_' + processId:
                this.processesData[processId].name = eventValue;
                break;
            case 'processStep_' + processId:
                this.processesData[processId].step = parseInt(eventValue);
                break;
            case 'processExecutionTime_' + processId:
                this.processesData[processId].executionTime = parseInt(eventValue);
                break;
            case 'processToFinishFirst_' + processId:
                if (eventValue.includes(',')) {
                    this.processesData[processId].finishFirst = eventValue.split(',');
                } else {
                    this.processesData[processId].finishFirst = eventValue;
                }

                break;
        }

        event.preventDefault();
    }

    /**
     * Schickt alle Daten zu App.tsx.
     *
     * @param event
     */
    public handleFormSubmit(event: any): void {
        event.preventDefault();
        this.props.getNetworkPlanData(this.processesData);
    }

    /**
     * Rendert das Formular.
     */
    public render(): React.ReactNode {
        return (
            <div className={'createForm'}>
                <form onSubmit={this.handleFormSubmit}>
                    <input name={'processName_0'}
                           type={'text'}
                           placeholder={'Bezeichnung'}
                           onChange={this.handleProcess}
                    />
                    <input name={'processStep_0'}
                           type={'text'}
                           placeholder={'Prozessschritt'}
                           onChange={this.handleProcess}
                    />
                    <input name={'processExecutionTime_0'} type={'text'} placeholder={'Dauer in Stunden'}
                           onChange={this.handleProcess}/>
                    <input name={'processToFinishFirst_0'} type={'text'} placeholder={'Vorher zu beenden'}
                           onChange={this.handleProcess}/>
                    {this.processes}
                    <div className={'addProcess'} onClick={() => this.addProcess()}>
                        Prozess hinzufügen
                    </div>
                    <input className={'createNetworkPlan'} type={'submit'} value={'Netzplan erstellen'}/>
                </form>
            </div>
        );
    }
}