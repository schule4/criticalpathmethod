import React from 'react';
import './css/HeaderMenu.css';
import {Icon} from "semantic-ui-react";

/**
 * Interface für die Props.
 */
interface HeaderMenuProps {
    createNewNetworkPlan: () => void,
    editCurrentNetworkPlan: () => void
}

/**
 * Interface für den State.
 */
interface HeaderMenuState {
    lastUpdated: string
}

export default class HeaderMenu extends React.Component<HeaderMenuProps, HeaderMenuState> {

    /**
     * Konstruktor der Klasse.
     *
     * @param props
     */
    public constructor(props: any) {
        super(props);

        this.clearStorage = this.clearStorage.bind(this);
        this.editCurrentPlan = this.editCurrentPlan.bind(this);
    }

    /**
     * Leert den Storage und ruft die Methode auf, welche den Kontent zu dem Neuer-Netzplan-Menü ändert und neu rendert.
     */
    public clearStorage(): void {
        localStorage.clear();
        this.props.createNewNetworkPlan();
    }

    /**
     * Leert den Storage und ruft die Methode auf, welche den Kontent zu dem Editieren-Menü ändert und neu rendert.
     */
    public editCurrentPlan(): void {
        localStorage.clear();
        this.props.editCurrentNetworkPlan();
    }

    /**
     * Standard Render-Methode von React.
     * Rendert das HTML.
     */
    public render(): React.ReactNode {
        return (
            <div className={'HeaderMenu'}>
                <div className={'HeaderTitle'}>Critical Path Method</div>
                <div className={'HeaderPlaceholder'}/>
                <div className={'HeaderButtons'}>
                    <div className={'editPlan'} onClick={this.editCurrentPlan}>
                        <Icon name={'edit'} size={'large'}/>
                    </div>
                    <div className={'createNewPlan'} onClick={this.clearStorage}>
                        <Icon name={'plus'} size={'large'}/>
                    </div>
                </div>
            </div>
        );
    }
}