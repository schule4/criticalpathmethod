import React from 'react';
import './css/NetworkPlan.css';

/**
 * Interface für die Props.
 */
interface NetworkPlanProps {
    networkPlanData: any
}

/**
 * Interface für den State.
 */
interface NetworkPlanState {
    lastUpdated: string
}

export default class NetworkPlan extends React.Component<NetworkPlanProps, NetworkPlanState> {

    /**
     * HTML für den Netzplan.
     */
    private networkPlan: React.ReactNode = <div/>;

    /**
     * Array, welches die vorherigen Prozesse beinhaltet.
     */
    private previousProcesses: Array<any> = [];

    /**
     * Array, welches die Spalten für den Netzplan beinhaltet.
     */
    private networkPlanColumns: Array<any> = [];

    /**
     * Array, welches alle bereits benutzen Prozesse beinhaltet.
     * Wird benutzt, um zu prüfen, dass keine Prozesse doppelt vorhanden sind.
     */
    private usedProcessesForColumns: Array<any> = [];

    /**
     * Konstruktor der Klasse.
     * Ruft die benötigten Methoden auf.
     *
     * @param props
     */
    constructor(props: any) {
        super(props);

        this.state = {
            lastUpdated: new Date().toLocaleString()
        };

        this.setFirstColumn();
        this.setColumns();
        this.createProcesses();
    }

    /**
     * Setzt die erste Spalte von dem Netzplan.
     */
    public setFirstColumn(): void {
        let columnArray: Array<object> = [];

        this.props.networkPlanData.forEach((process: any) => {
            if (process.previousProcesses === '-' && this.usedProcessesForColumns.includes(process) === false) {
                columnArray.push(process);
                this.usedProcessesForColumns.push(process);
            }
        });

        this.networkPlanColumns.push(columnArray);
    }

    /**
     * Setzt die ganzen restlichen Spalten von dem Netzplan.
     */
    public setColumns(): void {

        console.log("COLUMN ARRAY");
        console.log(this.networkPlanColumns);

        let columnArray: Array<any> = [];

        this.networkPlanColumns.forEach((columnProcesses: any) => {
            columnProcesses.forEach((columnProcess: any) => {
                this.props.networkPlanData.forEach((process: any) => {
                    if (columnProcess.nextProcesses.includes(process.step) && this.usedProcessesForColumns.includes(process) === false) {
                        columnArray.push(process);
                        this.usedProcessesForColumns.push(process);
                    }
                });
            });
        });

        this.networkPlanColumns.push(columnArray);

        if (columnArray[columnArray.length - 1].nextProcesses.length !== 0) {
            this.setColumns();
        }
    }

    /**
     * Erstellt das HTML für die Spalten und die dazugehörigen Prozesse.
     */
    public createProcesses(): void {

        let columnId: number = 0;
        let columnLeftValue: number = -20;

        this.networkPlan = this.networkPlanColumns.map((column: any) => {
            columnId++;
            columnLeftValue = columnLeftValue + 20;
            return (
                <div className={'processColumn' + columnId + ' singleColumn'} style={{
                    'width': '20rem',
                    'height': '80vh',
                    'position': 'absolute',
                    'left': columnLeftValue + 'rem'
                }}>
                    {this.createSingleProcess(column)}
                </div>
            );
        });

        this.setState({lastUpdated: new Date().toLocaleString()});
    }

    /**
     * Erstellt das HTML für die einzelnen Prozesse.
     *
     * @param processArray
     */
    public createSingleProcess(processArray: Array<any>): React.ReactNode {
        return (processArray.map((process: any, index: number) => {

                let processClassForCritical: string = '';

                if (process.isCritical) {
                    processClassForCritical = 'critical';
                }

                return (
                    <div className={'process ' + processClassForCritical} key={index}>
                        <div className={'firstColumn'}>
                            <div className={'faz'}>{process.faz}</div>
                            <div className={'id'}>{process.step}</div>
                            <div className={'executionTime'}>{process.executionTime}</div>
                            <div className={'saz'}>{process.saz}</div>
                        </div>
                        <div className={'secondColumn'}>
                            <div className={'fez'}>{process.fez}</div>
                            <div className={'name'}>{process.name}</div>
                            <div className={'gpfp'}>
                                <div className={'gp'}>{process.totalBuffer}</div>
                                <div className={'fp'}>{process.freeBuffer}</div>
                            </div>
                            <div className={'sez'}>{process.sez}</div>
                        </div>
                    </div>
                );
            })
        );
    }

    /**
     * Standard Render-Methode von React.
     * Rendert das HTML.
     */
    render(): React.ReactNode {
        return (this.networkPlan);
    }
}